# Introduction

The objectives of this practice are

- Understand data structures for table processing
- Process csv files
- Statistically describe a data set
- Visualize results using bar graphs and scatterplots

# Exercise

This [Site](http://www.properati.com.ar/) publishes information on rental and sale of real estate.

The file “properati-AR-2018-02-01-properties-sell.csv” contains information on the sale of real estate registered in
February 2018.


## Exercise 1

Write a program in python

1. Calculate the average value of 2-room apartments in Capital Federal
2. Make a bar graph by number of environments in Capital Federal, removing the outliers
3. Make a horizontal bar graph of the 10 neighborhoods with the greatest number of apartment publications. 2 room
in Federal Capital


## Exercise 2

For those properties in the Federal Capital that have geographic information, it is requested to write a program to make
a scatterplot of properties that differ by at most 0.05 degrees in latitude and longitude with respect to the geographic center of
the city

## Exercise 3

Make informed speculation about

The five cities with the largest population in the country are
- Buenos Aires
- Cordoba
- Rosario
- La Plata
- Mar del Plata

1. Considering only the departments of 3 environments, write a program that graphs a boxplot of the prices of those
departments. of the 5 cities mentioned.

2. Based on the previous graph
    - Which is the city with the highest cost of living?
    - Which is the most equitable city?
